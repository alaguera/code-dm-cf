import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation

U1 = 200
U2 = -100
dU = U1-U2


k = 0.1
cr = (U1+U2)/2
t = 0
dt = 0.001

A2 = -1
B1 = 1

xmin = 0
xmax = 100
ymin = -5
ymax = 5
points = 100
x = np.linspace(xmin,xmax,points)
y = np.linspace(ymin,ymax,points)

X,Y = np.meshgrid(x,y)




eta1x = -k*B1*np.sin(k*(X-cr*t))*np.exp(dU*t/2-Y)*(Y>0)
eta1y = -k*B1*np.cos(k*(X-cr*t))*np.exp(dU*t/2-Y)*(Y>0)

eta2x = k*A2*np.sin(k*(X-cr*t))*np.exp(dU*t/2+Y)*(Y<0)
eta2y = k*A2*np.cos(k*(X-cr*t))*np.exp(dU*t/2+Y)*(Y<0)

fig,ax = plt.subplots()
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
plt.xlabel('x')
plt.ylabel('y')
    
def animate(i):
    ax.collections = []
    ax.patches = []
    t = i*dt
    
    eta1x = -k*B1*np.sin(k*(X-cr*t))*np.exp(dU*t/2-Y)*(Y>0) + U1
    eta1y = -k*B1*np.cos(k*(X-cr*t))*np.exp(dU*t/2-Y)*(Y>0) 

    eta2x = k*A2*np.sin(k*(X-cr*t))*np.exp(dU*t/2+Y)*(Y<0) + U2
    eta2y = k*A2*np.cos(k*(X-cr*t))*np.exp(dU*t/2+Y)*(Y<0) 

    etax = eta1x + eta2x
    etay = eta1y + eta2y
    
    
    stream = ax.streamplot(X,Y,etax,etay, cmap = 'autumn', color = 'blue', density = 1, linewidth = 0.5, arrowsize = 1.5, arrowstyle = '->') 
    print(i)
    return(stream)
    
anim =   animation.FuncAnimation(fig, animate,  frames=500, blit=False, interval=20, repeat=True, repeat_delay = 500)

plt.show()